package com.demo.service;

import java.util.HashMap;
import java.util.Map;

import com.demo.domain.Product;

public class ProductCache {
	
	private  Map<String,Product> products = new HashMap<String,Product>();
	
	public ProductCache(){
		products.put("1201", new Product("1201", "Della latitude Laptop", 1389.0));
		products.put("1202", new Product("1202", "Chromecast", 29.0));
		products.put("1203", new Product("1203", "iphone 6s", 745.0));
	}
	
	public Product getDesiredProduct(String productId) {
		return products.get(productId);
	}

}
